use std::ffi::CStr;
use std::io;
use std::os::fd::{AsRawFd, FromRawFd, OwnedFd};

use libc::off_t;

#[cfg(not(any(target_os = "freebsd", all(target_os = "linux", feature = "memfd"))))]
pub fn create_shmem<T: AsRef<CStr>>(name: T, length: usize) -> io::Result<OwnedFd> {
    unsafe {
        let fd = make_fd(libc::shm_open(
            name.as_ref().as_ptr(),
            libc::O_CREAT | libc::O_RDWR | libc::O_EXCL,
            0o600,
        ))?;

        if libc::shm_unlink(name.as_ref().as_ptr()) == -1 {
            return Err(io::Error::last_os_error());
        }

        if libc::ftruncate(fd.as_raw_fd(), length as off_t) == -1 {
            return Err(io::Error::last_os_error());
        }

        Ok(fd)
    }
}

#[cfg(target_os = "freebsd")]
pub fn create_shmem<T: AsRef<CStr>>(_name: T, length: usize) -> io::Result<OwnedFd> {
    unsafe {
        let fd = make_fd(libc::shm_open(
            libc::SHM_ANON,
            libc::O_CREAT | libc::O_RDWR | libc::O_EXCL,
            0o600,
        ))?;

        if libc::ftruncate(fd, length as off_t) == -1 {
            return Err(io::Error::last_os_error());
        }

        Ok(fd)
    }
}

#[cfg(all(feature = "memfd", target_os = "linux"))]
pub fn create_shmem<T: AsRef<CStr>>(name: T, length: usize) -> io::Result<OwnedFd> {
    unsafe {
        let fd = make_fd(libc::memfd_create(
            name.as_ref().as_ptr(),
            libc::MFD_CLOEXEC,
        ))?;

        if libc::ftruncate(fd.as_raw_fd(), length as off_t) == -1 {
            return Err(io::Error::last_os_error());
        }

        Ok(fd)
    }
}

unsafe fn make_fd(fd: libc::c_int) -> io::Result<OwnedFd> {
    if fd == -1 {
        Err(io::Error::last_os_error())
    } else {
        Ok(unsafe { OwnedFd::from_raw_fd(fd) })
    }
}

#[cfg(test)]
mod tests {
    use std::ffi::CString;

    #[test]
    fn test_create_shmem() {
        super::create_shmem(CString::new("/helloworld").unwrap(), 1024).unwrap();
    }
}
